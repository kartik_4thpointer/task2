import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor() { }

  tableDataFromUser = {
    tableHeader: ['name', 'class', 'gender', 'email', 'location', 'action'],
    tableData: [
      { name: 'a', class: 'first', gender: 'male', email: 'a@gmail.com', location : 'delhi' , action : ['add', 'delete'] },
      { name: 'b', class: 'second', gender: 'female', email: 'b@gmail.com', location : 'mumbai',  action : ['add', 'delete']},
      { name: 'c', class: 'third', gender: 'male', email: 'c@gmail.com', location : 'delhi', action : ['add', 'delete']},
      { name: 'd', class: 'fourth', gender: 'female', email: 'd@gmail.com', location : 'mumbai', action : ['add', 'delete']},
      { name: 'e', class: 'five', gender: 'male', email: 'e@gmail.com', location : 'uk', action : ['add', 'delete']}
     ]
  };

  tableDataFromUserCopy = this.tableDataFromUser.tableData;

  ngOnInit() {
    console.log('tableDataFromUser', this.tableDataFromUser);
  }

  searchTextPassedCaptured(eventCaptured): void {
    console.log('searchTextPassedCaptured', eventCaptured);
    if (eventCaptured === '') {
      console.log('empty search', eventCaptured, this.tableDataFromUserCopy, this.tableDataFromUser.tableData);
      this.tableDataFromUser.tableData = this.tableDataFromUserCopy;
    } else {
      this.tableDataFromUser.tableData = this.tableDataFromUserCopy;
      this.tableDataFromUser.tableData = this.tableDataFromUser.tableData.filter((obj: any) => {
      return (obj.gender === eventCaptured || obj.class === eventCaptured || obj.name === eventCaptured ||
      obj.email === eventCaptured || obj.location === eventCaptured);
     }
    );
    }
  }

}
