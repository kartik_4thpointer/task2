import { Component, OnInit, Input, OnChanges, DoCheck } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, DoCheck {

  constructor() { }

  @Input() tableData;
  ngOnInit() {
  }

  // ngOnChanges() {
  // }

  ngDoCheck() {
    console.log('tableData', this.tableData);
  }
}
