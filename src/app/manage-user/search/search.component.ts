import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor() { }

  searchText: string;
  @Output() searchTextPassed = new EventEmitter();
  ngOnInit() {
    console.log('searchTextPassed', this.searchTextPassed);
  }

  searchTextCall(eventCapture) {
    console.log('eventCapture', eventCapture, eventCapture.target.value);
    this.searchTextPassed.emit(this.searchText);
  }
}
